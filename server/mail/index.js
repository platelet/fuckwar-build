'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _configEnvironment = require('../config/environment');

var _configEnvironment2 = _interopRequireDefault(_configEnvironment);

var transport_address = 'smtps://' + _configEnvironment2['default'].mail.username + ':' + _configEnvironment2['default'].mail.password + '@' + _configEnvironment2['default'].mail.outgoing_host;

var transporter = _nodemailer2['default'].createTransport(transport_address);

var hbs = require('nodemailer-express-handlebars');
var handlebars = hbs({
  viewEngine: {
    layoutsDir: "server/mail/layouts/",
    partialsDir: "server/mail/views/",
    defaultLayout: "mainLayout"
  },
  viewPath: "server/mail/views"
});
transporter.use('compile', handlebars);

function sendWelcomeEmail(_transporter) {
  var _transporter = _transporter;
  return function (to) {
    var mailOptions = {
      from: '"Stop the Arms Trade" <stop-the-arms-trade@riseup.net>',
      replyTo: '"Peace Action Wellington" <peacewellington@riseup.net>',
      to: to,
      subject: 'Thanks for joining the blockade to stop the Weapons Expo - spread the word',
      template: 'welcome',
      context: {}
    };
    _transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        return;
      }
      console.log('Message sent: ' + info.response);
    });
  };
}

function mailWrapper(_transporter) {
  var _transporter = _transporter;
  return function (to, subject, text, html) {
    var mailOptions = {
      from: '"Stop the Arms Trade" <stop-the-arms-trade@riseup.net>',
      to: to,
      subject: subject,
      text: text,
      html: html
    };
    _transporter.sendMail(mailOptions, function (error, info) {
      if (error) console.log(error);
      console.log('Message sent: ' + info.response);
    });
  };
}

exports.sendWelcomeEmail = sendWelcomeEmail(transporter);
exports.sendEmail = mailWrapper(transporter);
//# sourceMappingURL=index.js.map
