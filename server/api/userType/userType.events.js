/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2016 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * UserType model events
 */

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _events = require('events');

var UserType = require('./userType.model');
var UserTypeEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
UserTypeEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  UserType.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    UserTypeEvents.emit(event + ':' + doc._id, doc);
    UserTypeEvents.emit(event, doc);
  };
}

exports['default'] = UserTypeEvents;
module.exports = exports['default'];
//# sourceMappingURL=userType.events.js.map
