/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2016 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _supertest = require('supertest');

var _supertest2 = _interopRequireDefault(_supertest);

var app = require('../..');

var newUserType;

describe('UserType API:', function () {

  describe('GET /api/userTypes', function () {
    var userTypes;

    beforeEach(function (done) {
      (0, _supertest2['default'])(app).get('/api/userTypes').expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        userTypes = res.body;
        done();
      });
    });

    it('should respond with JSON array', function () {
      userTypes.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/userTypes', function () {
    beforeEach(function (done) {
      (0, _supertest2['default'])(app).post('/api/userTypes').send({
        name: 'New UserType',
        info: 'This is the brand new userType!!!'
      }).expect(201).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        newUserType = res.body;
        done();
      });
    });

    it('should respond with the newly created userType', function () {
      newUserType.name.should.equal('New UserType');
      newUserType.info.should.equal('This is the brand new userType!!!');
    });
  });

  describe('GET /api/userTypes/:id', function () {
    var userType;

    beforeEach(function (done) {
      (0, _supertest2['default'])(app).get('/api/userTypes/' + newUserType._id).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        userType = res.body;
        done();
      });
    });

    afterEach(function () {
      userType = {};
    });

    it('should respond with the requested userType', function () {
      userType.name.should.equal('New UserType');
      userType.info.should.equal('This is the brand new userType!!!');
    });
  });

  describe('PUT /api/userTypes/:id', function () {
    var updatedUserType;

    beforeEach(function (done) {
      (0, _supertest2['default'])(app).put('/api/userTypes/' + newUserType._id).send({
        name: 'Updated UserType',
        info: 'This is the updated userType!!!'
      }).expect(200).expect('Content-Type', /json/).end(function (err, res) {
        if (err) {
          return done(err);
        }
        updatedUserType = res.body;
        done();
      });
    });

    afterEach(function () {
      updatedUserType = {};
    });

    it('should respond with the updated userType', function () {
      updatedUserType.name.should.equal('Updated UserType');
      updatedUserType.info.should.equal('This is the updated userType!!!');
    });
  });

  describe('DELETE /api/userTypes/:id', function () {

    it('should respond with 204 on successful removal', function (done) {
      (0, _supertest2['default'])(app)['delete']('/api/userTypes/' + newUserType._id).expect(204).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });

    it('should respond with 404 when userType does not exist', function (done) {
      (0, _supertest2['default'])(app)['delete']('/api/userTypes/' + newUserType._id).expect(404).end(function (err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  });
});
//# sourceMappingURL=userType.integration.js.map
