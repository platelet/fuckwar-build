/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2016 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var userTypeCtrlStub = {
  index: 'userTypeCtrl.index',
  show: 'userTypeCtrl.show',
  create: 'userTypeCtrl.create',
  update: 'userTypeCtrl.update',
  destroy: 'userTypeCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  'delete': sinon.spy()
};

// require the index with our stubbed out modules
var userTypeIndex = proxyquire('./index.js', {
  'express': {
    Router: function Router() {
      return routerStub;
    }
  },
  './userType.controller': userTypeCtrlStub
});

describe('UserType API Router:', function () {

  it('should return an express router instance', function () {
    userTypeIndex.should.equal(routerStub);
  });

  describe('GET /api/userTypes', function () {

    it('should route to userType.controller.index', function () {
      routerStub.get.withArgs('/', 'userTypeCtrl.index').should.have.been.calledOnce;
    });
  });

  describe('GET /api/userTypes/:id', function () {

    it('should route to userType.controller.show', function () {
      routerStub.get.withArgs('/:id', 'userTypeCtrl.show').should.have.been.calledOnce;
    });
  });

  describe('POST /api/userTypes', function () {

    it('should route to userType.controller.create', function () {
      routerStub.post.withArgs('/', 'userTypeCtrl.create').should.have.been.calledOnce;
    });
  });

  describe('PUT /api/userTypes/:id', function () {

    it('should route to userType.controller.update', function () {
      routerStub.put.withArgs('/:id', 'userTypeCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/userTypes/:id', function () {

    it('should route to userType.controller.update', function () {
      routerStub.patch.withArgs('/:id', 'userTypeCtrl.update').should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/userTypes/:id', function () {

    it('should route to userType.controller.destroy', function () {
      routerStub['delete'].withArgs('/:id', 'userTypeCtrl.destroy').should.have.been.calledOnce;
    });
  });
});
//# sourceMappingURL=index.spec.js.map
