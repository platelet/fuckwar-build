'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _shaJs = require('sha.js');

var _shaJs2 = _interopRequireDefault(_shaJs);

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var ObjectId = mongoose.Schema.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var UserSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  _hash: String,
  _salt: String,
  email: String,
  role: String,
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Document Level Methods
 */
UserSchema.methods.verifyPassword = function (password) {
  var hashed_pass = (0, _shaJs2['default'])('sha256').update(String(password.toLowerCase()) + this._salt, 'utf-8').digest('hex');
  if (this._hash === hashed_pass) return true;
  return false;
};

UserSchema.virtual('fullname').get(function () {
  return this.firstName + ' ' + this.lastName;
});

exports['default'] = mongoose.model('User', UserSchema);
module.exports = exports['default'];
//# sourceMappingURL=user.model.js.map
