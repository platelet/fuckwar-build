/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/users              ->  index
 * POST    /api/users              ->  create
 * GET     /api/users/:id          ->  show
 * PUT     /api/users/:id          ->  update
 * DELETE  /api/users/:id          ->  destroy
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.index = index;
exports.login = login;
exports.logout = logout;
exports.current = current;
exports.show = show;
exports.create = create;
exports.createStudents = createStudents;
exports.resetCode = resetCode;
exports.setTopicLevel = setTopicLevel;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _userModel = require('./user.model');

var _userModel2 = _interopRequireDefault(_userModel);

var _utils = require('../../utils');

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _shaJs = require('sha.js');

var _shaJs2 = _interopRequireDefault(_shaJs);

var ObjectId = _mongoose2['default'].Types.ObjectId;

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      delete entity._hash;
      delete entity._salt;
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2['default'].extend(entity, updates);
    return updated.saveAsync().spread(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function ensureCanModify(_id, res) {
  return function (entity) {
    if (String(entity.info.teacher) === String(_id)) return entity;
    res.status(401).end();
    return null;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Users

function index(req, res) {
  _userModel2['default'].find({}).select('-_salt -_hash').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function login(req, res) {
  _userModel2['default'].findOne({ _id: req.user.id }, '-_salt -_hash').execAsync().then(respondWithResult(res))['catch'](handleError(res));
}

function logout(req, res) {
  req.logout();
  res.redirect('/');
}

function current(req, res) {
  if (req.user) {
    respondWithResult(res)(req.user);
  } else {
    res.json(false);
  }
}

// Gets a single User from the DB

function show(req, res) {
  if (req.user.userType.title === 'admin') {
    _userModel2['default'].findById(req.params.id).select('-_salt -_hash').execAsync().then(handleEntityNotFound(res)).then(respondWithResult(res))['catch'](handleError(res));
  } else {
    respondWithResult(res)([]);
  }
}

// Creates a new User in the DB

function create(req, res) {
  var salt = (0, _utils.salter)();
  var _hash = (0, _shaJs2['default'])('sha256').update(String(req.body.password) + salt, 'utf-8').digest('hex');
  req.body._hash = _hash;
  req.body._salt = salt;
  delete req.body.password;
  delete req.body.type;
  _userModel2['default'].createAsync(req.body).then(respondWithResult(res, 201))['catch'](handleError(res));
}

function createStudents(req, res) {
  if (req.user.userType.title === 'teacher') {
    UserType.findOne({ title: 'student' }, function (e, t) {
      var objects = req.body.map(function (e, i) {
        var name = e.split(' ');
        var salt = (0, _utils.salter)();
        return {
          firstName: name[0].trim(),
          lastName: name[1].trim(),
          userType: ObjectId(t._id),
          info: {
            levels: [],
            teacher: ObjectId(req.user._id)
          },
          _salt: salt,
          _hash: (0, _shaJs2['default'])('sha256').update(String(name[0].trim()) + salt, 'utf-8').digest('hex'),
          code: studentCode()
        };
      });
      _userModel2['default'].collection.insertAsync(objects).then(respondWithResult(res, 201))['catch'](handleError(res));
    });
  }
}

function resetCode(req, res) {
  if (req.user.userType.title === 'teacher') {
    _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(ensureCanModify(req.user._id, res)).then(saveUpdates({ code: studentCode() })).then(respondWithResult(res))['catch'](handleError(res));
  } else if (req.user.userType.title === 'admin') {
    _userModel2['default'].findById(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates({ code: studentCode() })).then(respondWithResult(res))['catch'](handleError(res));
  }
}

function setTopicLevel(req, res) {
  if (req.user._id.toString() !== req.params.id.toString()) {
    handleError(res, 403)('Permission Denied');
  } else {
    var info = {};
    _userModel2['default'].findById(req.params.id).then(handleEntityNotFound(res)).then(function (user) {
      info = user.info;
      if (info.topics) {
        info.topics[req.body.topicId] = {
          currentLevel: parseInt(req.body.level),
          unlockedLevels: Array.apply(null, { length: parseInt(req.body.level) + 2 }).map(function (e, i) {
            return i + 1;
          })
        };
      } else {
        info.topics = {};
        info.topics[req.body.topicId] = {
          currentLevel: parseInt(req.body.level),
          unlockedLevels: Array.apply(null, { length: parseInt(req.body.level) + 2 }).map(function (e, i) {
            return i + 1;
          })
        };
      }
      return saveUpdates({ info: info })(user);
    }).then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Updates an existing User in the DB

function update(req, res) {
  if (req.body.$loki) delete req.body.$loki;
  if (req.body.meta) delete req.body.meta;
  if (req.body._hash) delete req.body._hash;
  if (req.body._salt) delete req.body._salt;

  if (req.body.password && req.body.password.length > 3) {
    var salt = (0, _utils.salter)();
    var _hash = (0, _shaJs2['default'])('sha256').update(String(req.body.password) + salt, 'utf-8').digest('hex');
    req.body._hash = _hash;
    req.body._salt = salt;
  }

  req.body.userType = ObjectId(req.body.userType._id);
  if (req.body.info) {
    if (req.body.info.teacher) req.body.info.teacher = ObjectId(req.body.info.teacher._id);
  }

  if (req.user.userType.title === 'teacher') {
    _userModel2['default'].findById(req.body._id).then(function (user) {
      if (String(user.info.teacher) !== String(req.user._id)) {
        handleError(res, 401)('permission denied');
        return;
      } else {
        if (req.body._id) delete req.body._id;
        _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
      }
    });
    if (req.body.dashboardEnabled) delete req.body.dashboardEnabled;
  } else if (req.user.userType.title !== 'admin') {
    if (req.user._id.toString() !== req.body._id.toString()) {
      handleError(res, 401)('permission denied');
      return;
    }
    if (req.body.dashboardEnabled) delete req.body.dashboardEnabled;

    if (req.body._id) delete req.body._id;

    _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
  } else {
    if (req.body._id) delete req.body._id;

    _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res))['catch'](handleError(res));
  }
}

// Deletes a User from the DB

function destroy(req, res) {
  if (req.user.userType.title === 'admin') {
    _userModel2['default'].findByIdAsync(req.params.id).then(handleEntityNotFound(res)).then(removeEntity(res))['catch'](handleError(res));
  }
}
//# sourceMappingURL=user.controller.js.map
