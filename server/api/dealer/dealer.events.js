/**
 * Dealer model events
 */

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _events = require('events');

var Dealer = require('./dealer.model');
var DealerEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
DealerEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Dealer.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    DealerEvents.emit(event + ':' + doc._id, doc);
    DealerEvents.emit(event, doc);
  };
}

exports['default'] = DealerEvents;
module.exports = exports['default'];
//# sourceMappingURL=dealer.events.js.map
