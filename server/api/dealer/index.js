'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _connectEnsureLogin = require('connect-ensure-login');

var _connectEnsureLogin2 = _interopRequireDefault(_connectEnsureLogin);

var express = require('express');
var controller = require('./dealer.controller');

var router = express.Router();
var ensureLoggedIn = _connectEnsureLogin2['default'].ensureLoggedIn;

exports['default'] = function (passport) {

  router.get('/', controller.index);
  router.get('/:id', controller.show);
  router.post('/', ensureLoggedIn('/login'), controller.create);
  router.put('/:id', ensureLoggedIn('/login'), controller.update);
  router.patch('/:id', ensureLoggedIn('/login'), controller.update);
  router['delete']('/:id', ensureLoggedIn('/login'), controller.destroy);
  return router;
};

module.exports = exports['default'];
//# sourceMappingURL=index.js.map
