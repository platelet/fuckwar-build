'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var DealerSchema = new _mongoose2['default'].Schema({
  name: String,
  shortDescription: String,
  longDescription: String,
  isNZDIAMember: Boolean,
  isNZSubsidiary: Boolean,
  isNZFounded: Boolean,
  isNZOwned: Boolean,
  industry: String,
  logo: String,
  website: String,
  CEO: String,
  yearEstablished: Number,
  parentCompany: {
    website: String,
    logo: String,
    name: String
  },
  offices: [{
    physicalAddress: String,
    postalAddress: String,
    location: {
      latitude: Number,
      longitude: Number
    },
    phones: [{ type: String }],
    faxes: [{ type: String }],
    emails: [{ type: String }]
  }],
  active: Boolean
});

exports['default'] = _mongoose2['default'].model('Dealer', DealerSchema);
module.exports = exports['default'];
//# sourceMappingURL=dealer.model.js.map
