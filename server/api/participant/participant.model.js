'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var ParticipantSchema = new _mongoose2['default'].Schema({
  name: {
    type: String,
    'default': ''
  },
  email: {
    type: String,
    'default': ''
  },
  phone: {
    type: String,
    'default': ''
  },
  region: {
    type: String,
    'default': ''
  },
  accommodation: { type: String, 'default': '' },
  accommodation_dates: {
    ninth: { type: Boolean, 'default': false },
    tenth: { type: Boolean, 'default': false },
    eleventh: { type: Boolean, 'default': false }
  },
  billet_beds: { type: Number, 'default': -1 },
  billet_suburb: { type: String, 'default': '' },
  transport: { type: String, 'default': '' },
  transport_seats: { type: String, 'default': '' },
  transport_from: { type: String, 'default': '' },
  transport_from_wel: { type: String, 'default': '' },
  transport_to_wel: { type: String, 'default': '' },
  diet_requirements: { type: String, 'default': '' },
  access_requirements: { type: String, 'default': '' },
  data_retain: {
    type: Boolean,
    'default': false
  },
  data_share: {
    type: Boolean,
    'default': false
  },
  paw_list: {
    type: Boolean,
    'default': false
  },
  createdAt: {
    type: Date,
    'default': Date.now
  },
  active: Boolean
});

exports['default'] = _mongoose2['default'].model('Participant', ParticipantSchema);
module.exports = exports['default'];
//# sourceMappingURL=participant.model.js.map
