/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2016 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _connectEnsureLogin = require('connect-ensure-login');

var _connectEnsureLogin2 = _interopRequireDefault(_connectEnsureLogin);

var express = require('express');
var controller = require('./participant.controller');

var router = express.Router();
var ensureLoggedIn = _connectEnsureLogin2['default'].ensureLoggedIn;

exports['default'] = function (passport) {

  router.get('/', ensureLoggedIn('/login'), controller.index);
  router.get('/:id', ensureLoggedIn('/login'), controller.show);
  router.post('/', controller.create);
  router.put('/:id', ensureLoggedIn('/login'), controller.update);
  router.patch('/:id', ensureLoggedIn('/login'), controller.update);
  router['delete']('/:id', ensureLoggedIn('/login'), controller.destroy);
  return router;
};

module.exports = exports['default'];
//# sourceMappingURL=index.js.map
