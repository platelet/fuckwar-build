/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2016 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/weapons-dev'
  },

  mail: {
    outgoing_host: 'mail.riseup.net',
    username: 'stop-the-arms-trade',
    password: 'PGF-6ZF-kVp-rQP'
  },

  // Seed database on startup
  seedDB: false

};
//# sourceMappingURL=development.js.map
