/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2016 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

// Production specific configuration
// =================================
module.exports = {
  // Server IP
  ip: process.env.OPENSHIFT_NODEJS_IP || process.env.IP || undefined,

  // Server port
  port: process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080,

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://' + process.env.MONGODB_USER + ':' + process.env.MONGODB_PASSWORD + '@mongodb/' + process.env.MONGODB_DATABASE || 'mongodb://localhost/fuckwar'
  },

  //Mail configuration
  mail: {
    outgoing_host: process.env.MAIL_SERVER_ADDRESS,
    username: process.env.MAIL_SERVER_USERNAME,
    password: process.env.MAIL_SERVER_PASSWORD
  },

  seedDB: false
};
//# sourceMappingURL=production.js.map
