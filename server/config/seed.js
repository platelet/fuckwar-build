/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2016 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _apiUserUserModel = require('../api/user/user.model');

var _apiUserUserModel2 = _interopRequireDefault(_apiUserUserModel);

var _utils = require('../utils');

var _shaJs = require('sha.js');

var _shaJs2 = _interopRequireDefault(_shaJs);

var salt_1 = (0, _utils.salter)();
var salt_2 = (0, _utils.salter)();

_apiUserUserModel2['default'].find({}).remove().then(function () {
  _apiUserUserModel2['default'].create({
    provider: 'local',
    name: 'Test User',
    email: 'test@example.com',
    _salt: salt_1,
    _hash: (0, _shaJs2['default'])('sha256').update(String('test') + salt_1, 'utf-8').digest('hex')
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'stop-the-arms-trade@riseup.net',
    _salt: salt_2,
    _hash: (0, _shaJs2['default'])('sha256').update(String('admininwarresist') + salt_2, 'utf-8').digest('hex')
  }).then(function () {
    console.log('finished populating users');
  });
});
//# sourceMappingURL=seed.js.map
