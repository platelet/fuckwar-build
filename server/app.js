/******************************************************************************
 *                                                                            *
 * Matic Web App                                                              *
 * (C) Copyright Maths Adventures 2015-2016 All right Reserved                *
 *                                                                            *
 * Author: Sheldon Levet (sheldon.levet@me.com) 2016                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 *                                                                            *
/******************************************************************************/

'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _configEnvironment = require('./config/environment');

var _configEnvironment2 = _interopRequireDefault(_configEnvironment);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _passportLocal = require('passport-local');

var _passportLocal2 = _interopRequireDefault(_passportLocal);

var _apiUserUserModelJs = require('./api/user/user.model.js');

var _apiUserUserModelJs2 = _interopRequireDefault(_apiUserUserModelJs);

_mongoose2['default'].Promise = require('bluebird');

var LocalStrategy = _passportLocal2['default'].Strategy;

// Connect to MongoDB
_mongoose2['default'].connect(_configEnvironment2['default'].mongo.uri, _configEnvironment2['default'].mongo.options);
_mongoose2['default'].connection.on('error', function (err) {
  console.error('MongoDB connection error: ' + err);
  process.exit(-1);
});

// Populate databases with sample data
if (_configEnvironment2['default'].seedDB) {
  require('./config/seed');
}

// Setup server
var app = (0, _express2['default'])();
var server = _http2['default'].createServer(app);
require('./config/express')(app);
app.use(_passport2['default'].initialize());
app.use(_passport2['default'].session());
require('./routes')(app, _passport2['default']);

//login middleware declaration -- LOCAL HTTP
_passport2['default'].use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, function (req, username, password, done) {
  _apiUserUserModelJs2['default'].findOne({ email: username }, function (err, user) {
    if (err) {
      console.log(err);return done(err);
    }
    if (!user) {
      console.log('no user');return done(null, false);
    }
    if (!user.verifyPassword(password)) {
      console.log('wrong pass');return done(null, false);
    }
    return done(null, user);
  });
}));

_passport2['default'].serializeUser(function (user, done) {
  done(null, user._id);
});

_passport2['default'].deserializeUser(function (id, done) {
  _apiUserUserModelJs2['default'].findOne({ _id: id }).select('firstName lastName email info userType settings dashboardEnabled createdAt').populate('userType', 'title permissionEnum redirectTo').execAsync().then(function (user) {
    done(null, user);
  })['catch'](done);
});

// Start server
function startServer() {
  try {
    _fs2['default'].mkdirSync(_path2['default'].join(__dirname + '/tmp_files'));
  } catch (err) {}

  app.angularFullstack = server.listen(_configEnvironment2['default'].port, _configEnvironment2['default'].ip, function () {
    console.log('Express server listening on %d, in %s mode', _configEnvironment2['default'].port, app.get('env'));
  });
}

setImmediate(startServer);

// Expose app
exports = module.exports = app;
//# sourceMappingURL=app.js.map
