'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _componentsErrors = require('./components/errors');

var _componentsErrors2 = _interopRequireDefault(_componentsErrors);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

exports['default'] = function (app, passport) {
  app.use('/api/dealers', require('./api/dealer')(passport));
  app.use('/api/userTypes', require('./api/userType'));
  app.use('/api/users', require('./api/user')(passport));
  app.use('/api/participants', require('./api/participant')(passport));
  app.route('/logout').get(function (req, res) {
    req.logout();
    res.redirect('/');
  });
  app.route('/:url(api|auth|components|app|bower_components|assets)/*').get(_componentsErrors2['default'][404]);

  app.route('/*').get(function (req, res) {
    res.sendFile(_path2['default'].resolve(app.get('appPath') + '/index.html'));
  });
};

module.exports = exports['default'];
//# sourceMappingURL=routes.js.map
